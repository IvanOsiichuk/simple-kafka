ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.7"

lazy val root = (project in file("."))
  .settings(
    name := "simple-kafka"
  )

val akkaVersion         = "2.6.14"
val akkaHttpVersion     = "10.1.12"
val alpakkaVersion      = "3.0.4"
val alpakkaKafkaVersion = "2.0.5"
val playVersion         = "2.9.2"

// TODO update dependencies
libraryDependencies ++= Seq(
  "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpakkaVersion,
  "com.typesafe.akka"  %% "akka-stream-kafka"       % alpakkaKafkaVersion,
  "com.typesafe.akka"  %% "akka-actor-typed"        % akkaVersion,
  "com.typesafe.akka"  %% "akka-stream"             % akkaVersion,
  "com.typesafe.akka"  %% "akka-http"               % akkaHttpVersion,
  "com.typesafe.akka"  %% "akka-http-spray-json"    % akkaHttpVersion,
  "org.testcontainers" % "kafka"                    % "1.14.3",
  "com.typesafe.akka"  %% "akka-slf4j"              % akkaVersion,
  "ch.qos.logback"     % "logback-classic"          % "1.2.3",
  "com.typesafe.play"  %% "play-json"               % playVersion
)
