package iosiichuk.kafka.records

import play.api.libs.json.{Json, Reads, Writes}

case class JobMessage(name: String, status: String)

object JobMessage {
  implicit val formatWrite: Writes[JobMessage] = Json.writes[JobMessage]
  implicit val formatRead: Reads[JobMessage]   = Json.reads[JobMessage]
}
