package iosiichuk.kafka

import iosiichuk.kafka.service.{KafkaProps, WorkerService}

object OtherConsumerApp extends KafkaProps {

  val otherWorkerService =
    new WorkerService(bootstrapServer, "simple-kafka-group", 30)

  def main(args: Array[String]): Unit =
    otherWorkerService.readPartitionedAndCommit(defaultTopic)
}
