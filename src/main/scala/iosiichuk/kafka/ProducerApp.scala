package iosiichuk.kafka

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.scaladsl.adapter.TypedActorSystemOps
import akka.http.scaladsl.Http
import iosiichuk.kafka.records.JobMessage
import iosiichuk.kafka.service.KafkaProps

object ProducerApp extends KafkaProps {
  import actorSystem.executionContext

  def main(args: Array[String]): Unit = {
    jobService.send2AllPartition(JobMessage("first-job", "Done"), defaultTopic, maxPartitions)

    val cs: CoordinatedShutdown = CoordinatedShutdown(actorSystem)
    cs.addTask(CoordinatedShutdown.PhaseServiceStop, "shut-down-client-http-pool")(() =>
      Http()(actorSystem.toClassic).shutdownAllConnectionPools().map(_ => Done))

    // TODO stop system
  }
}
