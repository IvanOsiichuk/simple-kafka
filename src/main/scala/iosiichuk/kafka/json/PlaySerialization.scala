package iosiichuk.kafka.json

import iosiichuk.kafka.records.JobMessage
import org.apache.kafka.common.serialization.{Deserializer, Serializer}
import play.api.libs.json.Json

final class MessageSerializer extends Serializer[JobMessage] {
  override def serialize(topic: String, data: JobMessage): Array[Byte] =
    Json.toBytes(Json.toJson(data))
}

final class MessageDeserializer extends Deserializer[JobMessage] {
  override def deserialize(topic: String, data: Array[Byte]): JobMessage =
    Json.parse(data).as[JobMessage]
}
