package iosiichuk.kafka.service

import akka.Done
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.scaladsl.Source
import iosiichuk.kafka.json.MessageSerializer
import iosiichuk.kafka.records.JobMessage
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

import scala.concurrent.Future

class JobService(bootstrapServers: String)(implicit val actorSystem: ActorSystem[Nothing]) {

  private val kafkaProducerSettings = ProducerSettings(actorSystem.toClassic, new StringSerializer, new MessageSerializer)
    .withBootstrapServers(bootstrapServers)

  def sendSeq(jobs: Seq[JobMessage], topic: String): Future[Done] =
    Source(jobs)
      .map { job =>
        new ProducerRecord[String, JobMessage](topic, null, job)
      }
      .runWith(Producer.plainSink(kafkaProducerSettings))

  def send(job: JobMessage, topic: String): Future[Done] =
    Source
      .single(job)
      .map(x => new ProducerRecord[String, JobMessage](topic, null, x))
      .runWith(Producer.plainSink(kafkaProducerSettings))

  def send2AllPartition(job: JobMessage, topic: String, partitionNumber: Int): Future[Done] =
    Source(0 to partitionNumber)
      .map(i => new ProducerRecord[String, JobMessage](topic, i, null, job))
      .runWith(Producer.plainSink(kafkaProducerSettings))
}
