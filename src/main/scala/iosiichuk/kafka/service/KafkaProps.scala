package iosiichuk.kafka.service

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

trait KafkaProps {
  implicit val actorSystem: ActorSystem[Nothing] =
    ActorSystem[Nothing](Behaviors.empty, "simple-kafka")

  val bootstrapServer = "PLAINTEXT://localhost:9092" // TODO move to config
  val defaultTopic    = "two-partitions"
  val maxPartitions   = 2

  val jobService    = new JobService(bootstrapServer)
  val workerService = new WorkerService(bootstrapServer, "simple-kafka-group", maxPartitions)
}
