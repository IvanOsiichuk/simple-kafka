package iosiichuk.kafka.service

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Committer, Consumer}
import akka.kafka.{CommitterSettings, ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.Sink
import iosiichuk.kafka.json.MessageDeserializer
import iosiichuk.kafka.records.JobMessage
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

class WorkerService(bootstrapServers: String, groupId: String, maxPartitions: Int)(
    implicit val actorSystem: ActorSystem[Nothing]) {
  require(maxPartitions > 0, "Maximum number of partition must be greater than zero")

  val kafkaConsumerSettings: ConsumerSettings[String, JobMessage] =
    ConsumerSettings(actorSystem.toClassic, new StringDeserializer, new MessageDeserializer)
      .withBootstrapServers(bootstrapServers)
      .withGroupId(groupId)
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

  val committerSettings: CommitterSettings = CommitterSettings(actorSystem)

  def readAndLog(topic: String): Consumer.DrainingControl[_] =
    Consumer
      .plainSource(kafkaConsumerSettings, Subscriptions.topics(topic))
      .map(msg => logJobMessage(msg.value()))
      .toMat(Sink.ignore)(DrainingControl.apply)
      .run()

  def logJobMessage(jobMessage: JobMessage): Unit =
    actorSystem.log.info(s"Job message:\n  name=${jobMessage.name}, status=${jobMessage.status}\n")

  def readAndCommit(topic: String): Consumer.DrainingControl[_] =
    Consumer
      .committableSource(kafkaConsumerSettings, Subscriptions.topics(topic))
      .map { msg =>
        logJobMessage(msg.record.value())
        msg.committableOffset
      }
      .via(Committer.flow(committerSettings.withMaxBatch(1)))
      .toMat(Sink.ignore)(DrainingControl.apply)
      .run()

  def readPartitioned(topic: String): Consumer.DrainingControl[_] =
    Consumer
      .plainPartitionedSource(kafkaConsumerSettings, Subscriptions.topics(topic))
      .flatMapMerge(maxPartitions, _._2)
      .map(msg => logJobMessage(msg.value()))
      .toMat(Sink.ignore)(DrainingControl.apply)
      .run()

  def readPartitionedAndCommit(topic: String): Consumer.DrainingControl[_] =
    Consumer
      .committablePartitionedSource(kafkaConsumerSettings, Subscriptions.topics(topic))
      .flatMapMerge(maxPartitions, _._2)
      .map { msg =>
        logJobMessage(msg.record.value())
        msg.committableOffset
      }
      .toMat(Sink.ignore)(DrainingControl.apply)
      .run()
}
